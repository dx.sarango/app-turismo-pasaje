import 'package:flutter/material.dart';

class ParallaxWidgetLinearGradient extends StatelessWidget {
  const ParallaxWidgetLinearGradient({
    Key key,
    @required this.top,
    @required this.isVisible,
  }) : super(key: key);

  final double top;
  final bool isVisible;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Align(
            alignment: Alignment.bottomLeft,
            child: AnimatedOpacity(
              opacity: isVisible ? 1.0 : 0.0,
              duration: const Duration(milliseconds: 200),
              child: Container(
                height: MediaQuery.of(context).size.height / 2,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomLeft,
                        stops: [0.2, 0.55],
                        colors: [Colors.white.withOpacity(0), Colors.white]
                    )
                ),
              ),
            )
        ),

      ),
    );
  }
}