import 'package:flutter/material.dart';

class ParallaxWidgetText extends StatelessWidget {
  const ParallaxWidgetText({
    Key key,
    @required this.top,
    @required this.isChange,
    @required this.description,
    @required this.title,
  }) : super(key: key);

  final double top;
  final String title;
  final String description;
  final bool isChange;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width - 20,
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AnimatedDefaultTextStyle(
                style: isChange
                    ? TextStyle(color: Colors.black)
                    : TextStyle(color: Colors.white),
                duration: const Duration(milliseconds: 200),
                child: Text(title, style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 1.3
                )),
              ),
              SizedBox(
                height: 15,
              ),
              AnimatedDefaultTextStyle(
                style: isChange
                    ? TextStyle(color: Colors.black.withOpacity(.5))
                    : TextStyle(color: Colors.white),
                duration: const Duration(milliseconds: 200),
                child: Text(
                  description,
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    height: 1.5,
                    fontSize: 14
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              )
            ],
          ),
          color: Colors.transparent
      ),
    );
  }
}