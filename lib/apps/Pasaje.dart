import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:youtube_player/youtube_player.dart';

import 'package:app_turismo_pasaje/components/CarouselWidget.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetImage.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetText.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetLinearGradient.dart';
import 'package:app_turismo_pasaje/components/loader.dart';

import 'package:app_turismo_pasaje/models/autoridad.dart';
import 'package:app_turismo_pasaje/models/municipio.dart';
import 'package:app_turismo_pasaje/models/parroquia.dart';

import 'package:app_turismo_pasaje/apps/ListaLugares.dart';

class Pasaje extends StatefulWidget {
  const Pasaje({
    Key key,
    @required this.apiBaseUrl,
  }) : super(key: key);

  final String apiBaseUrl;

  @override
  _Pasaje createState() => _Pasaje();
}

class _Pasaje extends State<Pasaje> {
  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;

  String urlAutoridades = "";
  String urlMunicipio = "";
  String urlParroquias = "";

  Municipio dataMunicipio;
  Autoridad dataAutoridad;

  VideoPlayerController _videoController;

  @override
  void initState() {
    super.initState();
    urlAutoridades = "${widget.apiBaseUrl}/api/autoridades";
    urlMunicipio = "${widget.apiBaseUrl}/api/municipio";
    urlParroquias = "${widget.apiBaseUrl}/api/parroquias";
  }

  Future<Municipio> _getMunicipio() async {
    var response = await http.get(
        Uri.encodeFull(urlMunicipio),
        headers: { "Accept": "application/json" }
    );

    var convertDataToJson = jsonDecode(response.body);

    Municipio municipio = Municipio(convertDataToJson["id"], convertDataToJson["logo"], convertDataToJson["mision"], convertDataToJson["vision"], convertDataToJson["objetivos"], convertDataToJson["organigrama"], convertDataToJson["video"], convertDataToJson["portada"], convertDataToJson["subportada"]);

    return municipio;
  }

  Future<Autoridad> _getAutoridad() async {
    var response = await http.get(
        Uri.encodeFull(urlAutoridades),
        headers: { "Accept": "application/json" }
    );

    var convertDataToJson = jsonDecode(response.body);

    Autoridad autoridad = Autoridad(convertDataToJson["id"], convertDataToJson["nombre"], convertDataToJson["apellido"], convertDataToJson["imagen"], convertDataToJson["descripcion"]);

    return autoridad;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Stack(
          children: <Widget>[
            new ParallaxWidgetImage(top: topTwo, asset: "test"),
            new ParallaxWidgetLinearGradient(top: topThree, isVisible: isVisible),
            new ParallaxWidgetText(top: topOne, isChange: isChange, title: "Pasaje", description: "Información de las distintas parroquias que conforman el cantón junto con las indicaciones de cómo llegar desde diferentes puntos del país atravesó de Google Maps.",),
            ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                ),
                FutureBuilder(
                  future: _getMunicipio(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {

                    if (snapshot.data == null) {
                      return Container(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        color: Colors.white,
                        width: double.infinity,
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Loader(),
                                SizedBox(
                                  height: 5,
                                ),
                                Text("Cargando Lugares turísticos.", style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12,
                                ),),
                                Text("Espere por favor.", style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12,
                                ),),
                              ],
                            )
                        ),
                      );
                    } else {
                      Municipio municipio = snapshot.data;
                      dataMunicipio = municipio;

                      return Container(
                        color: Colors.white,
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left: 20.0, right: 20.0),
                              child: Center(
                                child: Text(
                                  "VISITA NUESTROS ATRACTIVOS",
                                  style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 1.2,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20.0, top: 10, right: 20),
                              child: Center(
                                child: Text(
                                  'Conoce la experiencia de vivir en medio la naturaleza. "El Paraiso escondido del sitio rio pindo de Uzhcurrumi"',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20.0, right: 20.0),
                              child: YoutubePlayer(
                                context: context,
                                source: dataMunicipio.video,
                                quality: YoutubeQuality.MEDIUM,
                                autoPlay: false,
                                showThumbnail: true,
                                // callbackController is (optional).
                                // use it to control player on your own.
                                callbackController: (controller) {
                                  _videoController = controller;
                                },
                              ),
                            ),
//                            importantPlaces.length == 0 ? Text("") : CarouselWidget(data: importantPlaces, pathImages: widget.apiBaseUrl,),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      );
                    }
                  },
                ),
                FutureBuilder(
                  future: _getAutoridad(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {

                    if (snapshot.data == null) {
                      return Container(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        color: Colors.white,
                        width: double.infinity,
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Loader(),
                                SizedBox(
                                  height: 5,
                                ),
                                Text("Cargando Lugares turísticos.", style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12,
                                ),),
                                Text("Espere por favor.", style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12,
                                ),),
                              ],
                            )
                        ),
                      );
                    } else {
                      Autoridad autoridad = snapshot.data;
                      dataAutoridad = autoridad;

                      return Container(
                        color: Colors.white,
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left: 20.0, right: 20.0),
                              child: Center(
                                child: Text(
                                  "BIENVENIDOS AL GAD PASAJE",
                                  style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 1.2,
                                    color: Colors.black,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20.0, top: 10, right: 20),
                              child: Center(
                                child: Text(
                                  "Conoce lo que la ciudad de la nieves ofrece a la comunidad de El Oro y El Ecuador.",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                                child: Card(
                                    elevation: 2,
                                    clipBehavior: Clip.hardEdge,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(3)
                                    ),
                                    margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
                                    child: Column(
//                                    fit: StackFit.expand,
                                      children: <Widget>[
                                        Container(
                                          width: 250,
                                          height: 300,
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                          ),
                                          child: FadeInImage.assetNetwork(
                                            placeholder: 'assets/loading-places.gif',
                                            image: "${widget.apiBaseUrl}/img/${autoridad.imagen}",
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "${autoridad.nombre} ${autoridad.apellido}",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 17,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 3,
                                              ),
                                              Text(
                                                "${autoridad.descripcion}",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 12,
                                                  color: Colors.redAccent,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 15,
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    )
                                )
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      );
                    }
                  },
                ),
                (dataMunicipio == null
                  ? Text("")
                  : Container(
                      color: Colors.white,
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 10),
                      child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: Center(
                            child: Text(
                              "INFORMACIÓN",
                              style: TextStyle(
                                fontSize: 16,
                                letterSpacing: 1.2,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20.0, top: 10, right: 20),
                          child: Center(
                            child: Text(
                              "Nuestra razón de ser, Pasaje un paraiso turístico!!!",
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                            child: Container(
                              color: Colors.redAccent,
                              padding: EdgeInsets.all(20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Misión",
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.white,
                                  ),
                                  Text(
                                    '${dataMunicipio.mision[0].toUpperCase()}${dataMunicipio.mision.substring(1).toString().toLowerCase()}',
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),

                        Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 20.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
                            child: Container(
                              color: Colors.blue,
                              padding: EdgeInsets.all(20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "Visión",
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.white,
                                  ),
                                  Text(
                                    '${dataMunicipio.vision[0].toUpperCase()}${dataMunicipio.vision.substring(1).toString().toLowerCase()}',
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  )
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}