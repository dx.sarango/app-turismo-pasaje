import 'package:app_turismo_pasaje/models/turismo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class LugarSeleccionado extends StatefulWidget {
  const LugarSeleccionado(
      {Key key, @required this.data, @required this.pathImages})
      : super(key: key);

  final Turismo data;
  final String pathImages;

  @override
  _LugarSeleccionadoState createState() => _LugarSeleccionadoState();
}

class _LugarSeleccionadoState extends State<LugarSeleccionado> {
  Turismo data;
  String pathImages;

  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 300,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      image: DecorationImage(
                        image: NetworkImage(
                          "$pathImages/img/${data.galeria[0]["imagen"]}",
                        ), fit: BoxFit.cover,
                      )),
                ),
                Container(
                  height: 300,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.black.withOpacity(0.3), Colors.white],
                        stops: [0.2, 1.0],
                      )),
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child:
                        Text(data.nombre, style: TextStyle(
                            color: Colors.white,
                            fontSize: 32,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 1.3
                        )),
                      ),
//                  IconButton(icon: Icon(Icons.photo_library, color: Colors.indigo,), onPressed: null)
                    ],
                  ),
                ),
              ],
            ),
            Container(
              height: MediaQuery.of(context).size.height - 300,
              child: FlutterMap(
                options: new MapOptions(
                  center: new LatLng(double.parse(data.latitud), double.parse(data.longitud)),
                  zoom: 13.0,
                  interactive: true
                ),
                layers: [
                  new TileLayerOptions(
                    urlTemplate: "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",
                    additionalOptions: {
                      'accessToken': 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
                      'id': 'mapbox.streets',
                    },
                  ),
                  new MarkerLayerOptions(
                    markers: [
                      new Marker(
                        width: 80.0,
                        height: 80.0,
                        point: new LatLng(double.parse(data.latitud), double.parse(data.longitud)),
                        builder: (ctx) =>
                        new Container(
                          child: new Icon(Icons.location_on, color: Colors.red, size: 50,),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
