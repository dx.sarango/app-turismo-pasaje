class Municipio {
  final id;
  final logo;
  final mision;
  final vision;
  final objetivos;
  final organigrama;
  final video;
  final portada;
  final subportada;

  Municipio(this.id, this.logo, this.mision, this.vision, this.objetivos,
      this.organigrama, this.video, this.portada, this.subportada);

}