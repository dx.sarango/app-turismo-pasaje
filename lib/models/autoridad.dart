class Autoridad {
  final id;
  final nombre;
  final apellido;
  final imagen;
  final descripcion;

  Autoridad(this.id, this.nombre, this.apellido, this.imagen, this.descripcion);

}