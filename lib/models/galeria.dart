class Galeria {
  final id;
  final imagen;
  final estado;

  Galeria(this.id, this.imagen, this.estado);
}